package test.permissionissue;

import android.Manifest;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST = 101;

    private static final String PERMISSION_1 = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final String PERMISSION_2 = Manifest.permission.READ_PHONE_STATE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnExtStorage    = (Button) findViewById(R.id.btn_ext_storage);
        Button btnReadPhone     = (Button) findViewById(R.id.btn_read_phone);
        Button btnBoth          = (Button) findViewById(R.id.btn_both);

        btnExtStorage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{PERMISSION_1}, MY_PERMISSIONS_REQUEST);
            }
        });

        btnReadPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{PERMISSION_2}, MY_PERMISSIONS_REQUEST);
            }
        });

        btnBoth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{PERMISSION_1, PERMISSION_2}, MY_PERMISSIONS_REQUEST);
            }
        });
    }
}
